<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>img_Product</name>
   <tag></tag>
   <elementGuidId>c9049d12-2d15-41d9-ad61-89d62faafcae</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//img[@alt='Picture of Copy of Computing and Internet EX']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>img[alt=&quot;Picture of Copy of Computing and Internet EX&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>img</value>
      <webElementGuid>db8e7e04-1092-4172-aeea-fd439531d267</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>alt</name>
      <type>Main</type>
      <value>Picture of Copy of Computing and Internet EX</value>
      <webElementGuid>a5ca5094-5c30-4ed2-9116-ad8c28ce1db2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>src</name>
      <type>Main</type>
      <value>https://demowebshop.tricentis.com/content/images/thumbs/0000209_copy-of-computing-and-internet-ex_125.jpeg</value>
      <webElementGuid>b573709e-aa4a-4f02-880e-82e70dd5abdf</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>Show details for Copy of Computing and Internet EX</value>
      <webElementGuid>72fa6e27-f29f-4226-9203-5d37032d9f00</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;master-wrapper-page&quot;]/div[@class=&quot;master-wrapper-content&quot;]/div[@class=&quot;master-wrapper-main&quot;]/div[@class=&quot;center-2&quot;]/div[@class=&quot;page category-page&quot;]/div[@class=&quot;page-body&quot;]/div[@class=&quot;product-grid&quot;]/div[@class=&quot;item-box&quot;]/div[@class=&quot;product-item&quot;]/div[@class=&quot;picture&quot;]/a[1]/img[1]</value>
      <webElementGuid>3d9107ab-9227-43b3-b788-24e58ab6f4f9</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:img</name>
      <type>Main</type>
      <value>//img[@alt='Picture of Copy of Computing and Internet EX']</value>
      <webElementGuid>169692d4-a610-4f41-ba4c-149b95d47608</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div/a/img</value>
      <webElementGuid>dbf01f5d-db4b-4a03-883b-e8dc376776f8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//img[@alt = 'Picture of Copy of Computing and Internet EX' and @src = 'https://demowebshop.tricentis.com/content/images/thumbs/0000209_copy-of-computing-and-internet-ex_125.jpeg' and @title = 'Show details for Copy of Computing and Internet EX']</value>
      <webElementGuid>18b4310d-b55b-4353-96cc-17dba5fcbb40</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
