<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_Sortby</name>
   <tag></tag>
   <elementGuidId>8e3ab221-8f9c-4d7d-9449-fc8018c64f43</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//select[@id='products-orderby']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#products-orderby</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>98249692-93d7-4b87-8e95-284304198904</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>products-orderby</value>
      <webElementGuid>3f5b6834-f84f-4e4d-8df1-234b87769b3f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>products-orderby</value>
      <webElementGuid>3aa32243-c8a2-4fa3-a088-fed3c7f4b0e5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onchange</name>
      <type>Main</type>
      <value>setLocation(this.value);</value>
      <webElementGuid>8f3ccc6e-9222-4dcb-9498-857054c63fb6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Position
Name: A to Z
Name: Z to A
Price: Low to High
Price: High to Low
Created on
</value>
      <webElementGuid>e6407307-4818-4fba-8ad4-b2159bac34d9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;products-orderby&quot;)</value>
      <webElementGuid>e45c7d3f-aeac-4053-b5a4-fd32cd186846</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//select[@id='products-orderby']</value>
      <webElementGuid>07d9d1d4-4c96-417f-9ce7-cdae0fd8ca51</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Sort by'])[1]/following::select[1]</value>
      <webElementGuid>da340435-122d-46a1-b99d-19bd01ac4cfc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='View as'])[1]/following::select[2]</value>
      <webElementGuid>6c80c2ec-347b-47ef-b5a1-911e754d9fc5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Display'])[1]/preceding::select[1]</value>
      <webElementGuid>8912e745-b4b4-485c-87c7-24b11664df97</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='per page'])[1]/preceding::select[2]</value>
      <webElementGuid>be28674a-06e2-4131-8e6d-e3d8fd6f16a8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/select</value>
      <webElementGuid>b2363c35-7474-48c5-a585-5a44fa9f0032</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[@id = 'products-orderby' and @name = 'products-orderby' and (text() = 'Position
Name: A to Z
Name: Z to A
Price: Low to High
Price: High to Low
Created on
' or . = 'Position
Name: A to Z
Name: Z to A
Price: Low to High
Price: High to Low
Created on
')]</value>
      <webElementGuid>b0e556af-5902-4775-8e26-d58a173f6cce</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
