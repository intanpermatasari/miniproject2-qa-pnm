<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>lbl_Filterbyprice</name>
   <tag></tag>
   <elementGuidId>897508b5-b0a4-45b0-9231-25fbf49bd455</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='per page'])[1]/following::strong[1]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.filter-title > strong</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>strong</value>
      <webElementGuid>eb25f489-f293-4e6f-8a69-8b129697bf9a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Filter by price</value>
      <webElementGuid>f9cba13a-6acb-4312-be78-bc2c5c46c61f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;master-wrapper-page&quot;]/div[@class=&quot;master-wrapper-content&quot;]/div[@class=&quot;master-wrapper-main&quot;]/div[@class=&quot;center-2&quot;]/div[@class=&quot;page category-page&quot;]/div[@class=&quot;page-body&quot;]/div[@class=&quot;product-filters-wrapper&quot;]/div[@class=&quot;product-filters price-range-filter&quot;]/div[@class=&quot;filter-title&quot;]/strong[1]</value>
      <webElementGuid>b242489a-b8b3-4a37-9b68-3e11ffa39d31</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='per page'])[1]/following::strong[1]</value>
      <webElementGuid>25c64abc-d135-4eb1-8bf7-c8ca515a32af</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Display'])[1]/following::strong[1]</value>
      <webElementGuid>585c20a9-e7ef-4b18-9e08-9541ff683b15</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Filter by price']/parent::*</value>
      <webElementGuid>29bfca6e-dd10-425b-8e2e-e43ecd6788b2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div/strong</value>
      <webElementGuid>c385f41a-8810-4ca6-b181-86e1c791b1b9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//strong[(text() = 'Filter by price' or . = 'Filter by price')]</value>
      <webElementGuid>816b8cb5-b93b-431a-9a60-e02c8346fa81</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
