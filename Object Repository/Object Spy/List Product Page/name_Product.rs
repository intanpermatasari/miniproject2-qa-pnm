<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>name_Product</name>
   <tag></tag>
   <elementGuidId>7a5afc6f-6f4d-461f-8bf5-47388ec386c9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a[contains(text(),'Copy of Computing and Internet EX')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>1d25a75d-222e-4941-9ae4-61698c09b115</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/copy-of-computing-and-internet</value>
      <webElementGuid>5aa9027d-5651-4710-9a8e-79d0c7a26a7e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Copy of Computing and Internet EX</value>
      <webElementGuid>8212f08a-40fc-473b-bc35-dd54f6dbd994</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;master-wrapper-page&quot;]/div[@class=&quot;master-wrapper-content&quot;]/div[@class=&quot;master-wrapper-main&quot;]/div[@class=&quot;center-2&quot;]/div[@class=&quot;page category-page&quot;]/div[@class=&quot;page-body&quot;]/div[@class=&quot;product-grid&quot;]/div[@class=&quot;item-box&quot;]/div[@class=&quot;product-item&quot;]/div[@class=&quot;details&quot;]/h2[@class=&quot;product-title&quot;]/a[1]</value>
      <webElementGuid>02a3ef82-e724-48a9-b738-c601c4d3a9bb</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Copy of Computing and Internet EX')]</value>
      <webElementGuid>ead43d86-7f48-4ba3-a818-4b33e9316ed7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='More Than 100 tips about computing and internet.'])[1]/following::a[2]</value>
      <webElementGuid>638b1356-8a08-417a-9402-1aa7e3f4bb6f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Computing and Internet'])[1]/following::a[2]</value>
      <webElementGuid>ae76faba-72a7-4a53-8bc6-de5edc8ae89c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='More Than 100 tips about computing and internet.'])[2]/preceding::a[1]</value>
      <webElementGuid>a41bdad9-6c84-4c1c-a03e-d2d969bb49d6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Fiction'])[1]/preceding::a[2]</value>
      <webElementGuid>56bdf35d-5b16-41b3-81e5-8f4914c058c0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Copy of Computing and Internet EX']/parent::*</value>
      <webElementGuid>c6f40369-051e-4917-86b1-8a670db0871e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>(//a[contains(@href, '/copy-of-computing-and-internet')])[2]</value>
      <webElementGuid>71b7fd79-f48b-4ddb-a462-45a1cb8bfed3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div[2]/h2/a</value>
      <webElementGuid>5b772b1d-c63e-47ac-bac9-ae7e1c36d314</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '/copy-of-computing-and-internet' and (text() = 'Copy of Computing and Internet EX' or . = 'Copy of Computing and Internet EX')]</value>
      <webElementGuid>9cdbe83c-826a-4710-aadd-fc9df7679323</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
