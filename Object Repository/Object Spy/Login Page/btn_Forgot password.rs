<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn_Forgot password</name>
   <tag></tag>
   <elementGuidId>d6121a62-9dfe-4163-9c40-91e38e19b3fe</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>span.forgot-password > a</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//a[contains(text(),'Forgot password?')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>72111399-e96c-4c70-93f2-3abf23669909</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/passwordrecovery</value>
      <webElementGuid>8202adec-2272-41ff-a07e-a64a2a97d198</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Forgot password?</value>
      <webElementGuid>84dcdcfd-cc70-41f9-b950-70255bd3715c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;master-wrapper-page&quot;]/div[@class=&quot;master-wrapper-content&quot;]/div[@class=&quot;master-wrapper-main&quot;]/div[@class=&quot;center-2&quot;]/div[@class=&quot;page login-page&quot;]/div[@class=&quot;page-body&quot;]/div[@class=&quot;customer-blocks&quot;]/div[@class=&quot;returning-wrapper&quot;]/div[@class=&quot;form-fields&quot;]/form[1]/div[@class=&quot;inputs reversed&quot;]/span[@class=&quot;forgot-password&quot;]/a[1]</value>
      <webElementGuid>20d634c4-7cf5-4194-9d12-6710dd9e1448</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Forgot password?')]</value>
      <webElementGuid>bdb7571c-e0e5-4e02-9934-b9d9bec90f39</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Remember me?'])[1]/following::a[1]</value>
      <webElementGuid>127dab38-1663-482c-8605-06ca0c44175e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Password:'])[1]/following::a[1]</value>
      <webElementGuid>a8c520a2-ee11-4a6d-a102-440e63ce1e0e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='About login / registration'])[1]/preceding::a[1]</value>
      <webElementGuid>fab69ec6-0d67-4826-9086-452ce3a21499</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Information'])[1]/preceding::a[1]</value>
      <webElementGuid>d2310be7-c678-4520-90f1-3f8cc61b9c44</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Forgot password?']/parent::*</value>
      <webElementGuid>b466439e-41c7-4300-a40e-edb946c0f2d7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, '/passwordrecovery')]</value>
      <webElementGuid>d5957486-b01d-4736-a3fd-457e6805594e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//span/a</value>
      <webElementGuid>18dcca52-9982-4b5d-b7aa-d242926dfca7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '/passwordrecovery' and (text() = 'Forgot password?' or . = 'Forgot password?')]</value>
      <webElementGuid>3829cf0d-3a03-4bb8-ac30-a955ae45153b</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
