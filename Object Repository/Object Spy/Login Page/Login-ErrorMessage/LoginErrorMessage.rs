<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>LoginErrorMessage</name>
   <tag></tag>
   <elementGuidId>a3565500-1c48-4ce6-8820-fde0e51459d6</elementGuidId>
   <imagePath></imagePath>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.validation-summary-errors</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Returning Customer'])[1]/following::div[3]</value>
      </entry>
      <entry>
         <key>IMAGE</key>
         <value></value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = 'Login was unsuccessful. Please correct the errors and try again.
The credentials provided are incorrect
' or . = 'Login was unsuccessful. Please correct the errors and try again.
The credentials provided are incorrect
')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>555f9937-61ad-4211-8dd7-de9904436714</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>validation-summary-errors</value>
      <webElementGuid>cc50ae5f-aef7-4698-ac25-b22016622385</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Login was unsuccessful. Please correct the errors and try again.
The credentials provided are incorrect
</value>
      <webElementGuid>cf8ee22b-24a0-48df-9fc2-4260b84c1b80</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;master-wrapper-page&quot;]/div[@class=&quot;master-wrapper-content&quot;]/div[@class=&quot;master-wrapper-main&quot;]/div[@class=&quot;center-2&quot;]/div[@class=&quot;page login-page&quot;]/div[@class=&quot;page-body&quot;]/div[@class=&quot;customer-blocks&quot;]/div[@class=&quot;returning-wrapper&quot;]/div[@class=&quot;form-fields&quot;]/form[1]/div[@class=&quot;message-error&quot;]/div[@class=&quot;validation-summary-errors&quot;]</value>
      <webElementGuid>0c670108-4f8d-4b24-8edd-f77cb41683d7</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Returning Customer'])[1]/following::div[3]</value>
      <webElementGuid>f3918501-a398-44ba-860c-b4339467de16</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='New Customer'])[1]/following::div[7]</value>
      <webElementGuid>6a5260c2-f17e-4940-90a2-62a10264c32a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//form/div/div</value>
      <webElementGuid>9c75ad23-9d2e-49f6-a495-c20a30f59434</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = 'Login was unsuccessful. Please correct the errors and try again.
The credentials provided are incorrect
' or . = 'Login was unsuccessful. Please correct the errors and try again.
The credentials provided are incorrect
')]</value>
      <webElementGuid>809b1988-9cf3-4c0b-b6e0-e51e4728c81f</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
