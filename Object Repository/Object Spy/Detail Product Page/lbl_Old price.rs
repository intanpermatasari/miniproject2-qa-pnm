<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>lbl_Old price</name>
   <tag></tag>
   <elementGuidId>49f8e70f-b361-4932-8f67-9d09123b9287</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//form[@id='product-details-form']/div/div/div[2]/div[6]/div/label</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>label</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>label</value>
      <webElementGuid>057c2377-c918-4bb5-a30e-be93ab6ec82d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Old price:</value>
      <webElementGuid>e4f047fd-36d6-4d85-89ad-912f8356d1c3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;product-details-form&quot;)/div[1]/div[@class=&quot;product-essential&quot;]/div[@class=&quot;overview&quot;]/div[@class=&quot;prices&quot;]/div[@class=&quot;old-product-price&quot;]/label[1]</value>
      <webElementGuid>2f614cea-5ea7-4f00-a594-94f78299c5bc</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='product-details-form']/div/div/div[2]/div[6]/div/label</value>
      <webElementGuid>b61e1cbc-b5c7-4b50-958a-ebcb26d56599</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Add your review'])[1]/following::label[1]</value>
      <webElementGuid>4ef7b7ae-8829-4354-bd4a-46ff1813ba46</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='|'])[1]/following::label[1]</value>
      <webElementGuid>a5cce146-9223-43b5-8af9-ff54c8308067</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Price:'])[1]/preceding::label[1]</value>
      <webElementGuid>8d2466f4-f6f3-4b81-97c7-c30903b58bc0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Qty:'])[1]/preceding::label[2]</value>
      <webElementGuid>f8badb4a-c1fc-4ee4-a618-86f1697a6abc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Old price:']/parent::*</value>
      <webElementGuid>1933d9d0-b01f-450b-8f25-2f82bcffecf5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//label</value>
      <webElementGuid>24790cb8-94cc-4936-b595-12f3dcc3f1b2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//label[(text() = 'Old price:' or . = 'Old price:')]</value>
      <webElementGuid>f856c55b-ff6a-4738-9dd5-ab9f8b97dd8c</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
