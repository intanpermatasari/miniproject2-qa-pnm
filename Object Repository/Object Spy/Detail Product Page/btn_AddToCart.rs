<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn_AddToCart</name>
   <tag></tag>
   <elementGuidId>770c9f1f-48ad-439d-9503-cb5ae4aef7de</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='add-to-cart-button-13']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#add-to-cart-button-13</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>80a40b08-e1c1-468c-9df2-a2b8c5601c8f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>3b28f85d-2b71-49ed-ac2c-12fd70c2e32f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>add-to-cart-button-13</value>
      <webElementGuid>c0b0534c-fc25-4673-8663-1c504b3d4656</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>button-1 add-to-cart-button</value>
      <webElementGuid>ec791439-68cf-4b75-830c-2455a47bdbbe</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>Add to cart</value>
      <webElementGuid>86e26213-6ca0-4d9f-95e1-6953982285f4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-productid</name>
      <type>Main</type>
      <value>13</value>
      <webElementGuid>e56a4935-f597-4b14-8d74-eb31fec7f44f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onclick</name>
      <type>Main</type>
      <value>AjaxCart.addproducttocart_details('/addproducttocart/details/13/1', '#product-details-form');return false;</value>
      <webElementGuid>0cbf50de-9bc5-4543-abaf-86ebb95b03f8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;add-to-cart-button-13&quot;)</value>
      <webElementGuid>9c567fa7-55a4-445f-b669-90ec2c5b3146</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='add-to-cart-button-13']</value>
      <webElementGuid>f6cdd438-d15f-4085-bb29-942e8c9aefdc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='product-details-form']/div/div/div[2]/div[7]/div/input[2]</value>
      <webElementGuid>96232cdf-cee1-4a2f-8f35-95c13dd3481d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/input[2]</value>
      <webElementGuid>bf252d8d-a4d9-4643-a083-29dee09d664b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'button' and @id = 'add-to-cart-button-13']</value>
      <webElementGuid>9131023f-1abd-43cc-aef4-5e487077004a</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
