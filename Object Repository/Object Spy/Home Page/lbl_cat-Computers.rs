<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>lbl_cat-Computers</name>
   <tag></tag>
   <elementGuidId>b46c5d6d-3cdb-4757-b181-591da6cc6a5a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>a.hover</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//a[contains(text(),'Computers')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>4818abb5-9468-4e36-a7d8-8c44d74157f7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/computers</value>
      <webElementGuid>d5405c4d-496c-41e3-90ef-732720119fc1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>hover</value>
      <webElementGuid>cec3072f-b2d3-432c-aa2c-851d57d53c55</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Computers
        </value>
      <webElementGuid>f999fb87-e7dd-4a6a-bb6c-bc3176841ac4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;master-wrapper-page&quot;]/div[@class=&quot;master-wrapper-content&quot;]/div[@class=&quot;header-menu&quot;]/ul[@class=&quot;top-menu&quot;]/li[2]/a[@class=&quot;hover&quot;]</value>
      <webElementGuid>b2c735ed-7b2f-4fe0-80ec-448b9a077702</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Computers')]</value>
      <webElementGuid>26f185b6-6515-4510-932d-cbe13bf40267</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Books'])[1]/following::a[1]</value>
      <webElementGuid>8a93cfc2-2b28-47b0-8d16-c1c47b901cae</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='You have no items in your shopping cart.'])[1]/following::a[2]</value>
      <webElementGuid>d77b29b9-c975-4ae6-802b-a976c285191b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Desktops'])[1]/preceding::a[1]</value>
      <webElementGuid>75f2745e-50db-4c45-a40f-eb8a36d76699</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Notebooks'])[1]/preceding::a[2]</value>
      <webElementGuid>e5240900-dc33-4ba1-a3b3-5443e4f4c47b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Computers']/parent::*</value>
      <webElementGuid>e3b74be3-5600-4269-98ce-dc1fd52c50d5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, '/computers')]</value>
      <webElementGuid>82e018e5-704d-4bcc-b884-9fd9248e4e25</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/ul/li[2]/a</value>
      <webElementGuid>bcbef980-d532-4910-ab60-d631c25dee0b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '/computers' and (text() = 'Computers
        ' or . = 'Computers
        ')]</value>
      <webElementGuid>ab21bdcd-2bbb-47b1-a361-a43db76f37b4</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
