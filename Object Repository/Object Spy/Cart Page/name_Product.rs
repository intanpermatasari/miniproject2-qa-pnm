<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>name_Product</name>
   <tag></tag>
   <elementGuidId>21a0d075-5d44-41ae-8aa9-a50f359d0433</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(//a[contains(text(),'Fiction')])[2]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>a.product-name</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>6b63fb81-88b7-459a-9bdf-731573d7065a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/fiction</value>
      <webElementGuid>b43a7fb7-c19c-48da-8e33-50328397ecb5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>product-name</value>
      <webElementGuid>927a14d3-4a1d-4760-a8c7-e7ba07fb00bf</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Fiction</value>
      <webElementGuid>209d20e3-fb59-4a0d-87ca-b47d7f8e0af4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;master-wrapper-page&quot;]/div[@class=&quot;master-wrapper-content&quot;]/div[@class=&quot;master-wrapper-main&quot;]/div[@class=&quot;center-1&quot;]/div[@class=&quot;page shopping-cart-page&quot;]/div[@class=&quot;page-body&quot;]/div[@class=&quot;order-summary-content&quot;]/form[1]/table[@class=&quot;cart&quot;]/tbody[1]/tr[@class=&quot;cart-item-row&quot;]/td[@class=&quot;product&quot;]/a[@class=&quot;product-name&quot;]</value>
      <webElementGuid>fa0caf95-ed06-4913-a0f1-3b1f21d75ee7</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>(//a[contains(text(),'Fiction')])[2]</value>
      <webElementGuid>badc73b1-b54e-4a01-b481-8e1e1cf35f5c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Remove:'])[1]/following::a[1]</value>
      <webElementGuid>11f1e97e-92c9-4c6b-8c5f-8090d92cad73</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Total'])[1]/following::a[1]</value>
      <webElementGuid>800a9a7d-8ba5-48d7-8890-78c94ddd14a3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Price:'])[1]/preceding::a[1]</value>
      <webElementGuid>666b9887-6c53-4d44-ac00-289049442534</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Qty.:'])[1]/preceding::a[1]</value>
      <webElementGuid>86c2b1c4-9b9d-4477-b410-b06d332c6e60</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>(//a[contains(@href, '/fiction')])[3]</value>
      <webElementGuid>1747641f-96a1-4d5c-abfa-6c6b1b12ab2f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//td[3]/a</value>
      <webElementGuid>7e4988a1-7826-488a-a660-9b2424d855c1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '/fiction' and (text() = 'Fiction' or . = 'Fiction')]</value>
      <webElementGuid>ff824be2-94af-4f30-ac51-6b47e215cf83</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
