<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>lbl_TotalChekout</name>
   <tag></tag>
   <elementGuidId>ae849d8f-30b1-4f4b-b241-537b4b22ff79</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Total:'])[2]/following::span[2]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>32fe615d-c737-41d8-a700-07bcb8a7dad8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Calculated during checkout</value>
      <webElementGuid>fbed9bb7-ad53-4fb6-90dd-fd5477adf4f6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;master-wrapper-page&quot;]/div[@class=&quot;master-wrapper-content&quot;]/div[@class=&quot;master-wrapper-main&quot;]/div[@class=&quot;center-1&quot;]/div[@class=&quot;page shopping-cart-page&quot;]/div[@class=&quot;page-body&quot;]/div[@class=&quot;order-summary-content&quot;]/form[1]/div[@class=&quot;cart-footer&quot;]/div[@class=&quot;totals&quot;]/div[@class=&quot;total-info&quot;]/table[@class=&quot;cart-total&quot;]/tbody[1]/tr[4]/td[@class=&quot;cart-total-right&quot;]/span[@class=&quot;nobr&quot;]/span[1]</value>
      <webElementGuid>df3af5e5-1baf-41b3-8fcc-7307b6718bcd</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Total:'])[2]/following::span[2]</value>
      <webElementGuid>fa4510f4-2410-4683-91be-1fa37813c6bb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Tax:'])[1]/following::span[5]</value>
      <webElementGuid>b9eea999-1cf3-4caa-a5dd-ffdd8f1aba6c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='(read)'])[1]/preceding::span[1]</value>
      <webElementGuid>a31bc84d-feba-49a8-bfed-672dd87abba9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//tr[4]/td[2]/span/span</value>
      <webElementGuid>d4ff4212-781f-49b0-a82c-0e0b7506e772</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Calculated during checkout' or . = 'Calculated during checkout')]</value>
      <webElementGuid>dac096f4-0e01-4f22-b04e-09f122f82ee5</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
