<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>img_Product</name>
   <tag></tag>
   <elementGuidId>79ce38ad-586a-48b4-9bed-5d29e3ba530a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(//img[@alt='Picture of Fiction'])[2]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>td.product-picture > img[alt=&quot;Picture of Fiction&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>img</value>
      <webElementGuid>7a26738b-217e-4003-be54-ef8452c85a1f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>alt</name>
      <type>Main</type>
      <value>Picture of Fiction</value>
      <webElementGuid>baf628d4-f025-4d0d-bd74-21264a45e25a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>src</name>
      <type>Main</type>
      <value>https://demowebshop.tricentis.com/content/images/thumbs/0000133_fiction_80.jpeg</value>
      <webElementGuid>e09b2ce2-6ab9-43cf-8576-2b3d3b25b907</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>Show details for Fiction</value>
      <webElementGuid>87025f80-0611-4c38-992a-87fb2a66604e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;master-wrapper-page&quot;]/div[@class=&quot;master-wrapper-content&quot;]/div[@class=&quot;master-wrapper-main&quot;]/div[@class=&quot;center-1&quot;]/div[@class=&quot;page shopping-cart-page&quot;]/div[@class=&quot;page-body&quot;]/div[@class=&quot;order-summary-content&quot;]/form[1]/table[@class=&quot;cart&quot;]/tbody[1]/tr[@class=&quot;cart-item-row&quot;]/td[@class=&quot;product-picture&quot;]/img[1]</value>
      <webElementGuid>cc1eebcf-0edf-468e-a5ee-613db65bc816</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:img</name>
      <type>Main</type>
      <value>(//img[@alt='Picture of Fiction'])[2]</value>
      <webElementGuid>146b13cc-a254-487c-ae5f-f6388c997b7a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//td[2]/img</value>
      <webElementGuid>e593d84d-d10a-44b1-bdb4-7bd66e96ee3a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//img[@alt = 'Picture of Fiction' and @src = 'https://demowebshop.tricentis.com/content/images/thumbs/0000133_fiction_80.jpeg' and @title = 'Show details for Fiction']</value>
      <webElementGuid>910b6e94-90a5-4d45-b0a1-3712a7d742c9</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
