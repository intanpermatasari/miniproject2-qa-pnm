<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>field_Product(s)</name>
   <tag></tag>
   <elementGuidId>8257952e-7757-42a5-be39-9e1542a0a9be</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Remove'])[1]/following::th[2]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>th</value>
      <webElementGuid>fb3ca2f9-8499-4bce-b96c-e18cd4419f0d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                        Product(s)
                    </value>
      <webElementGuid>cd120de3-1963-483f-8876-453852e3f05a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;master-wrapper-page&quot;]/div[@class=&quot;master-wrapper-content&quot;]/div[@class=&quot;master-wrapper-main&quot;]/div[@class=&quot;center-1&quot;]/div[@class=&quot;page shopping-cart-page&quot;]/div[@class=&quot;page-body&quot;]/div[@class=&quot;order-summary-content&quot;]/form[1]/table[@class=&quot;cart&quot;]/thead[1]/tr[@class=&quot;cart-header-row&quot;]/th[3]</value>
      <webElementGuid>29a4ec07-004f-4884-8e0e-443d96f82271</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Remove'])[1]/following::th[2]</value>
      <webElementGuid>09cf53b3-b57d-4458-9beb-0bea49f2c794</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Shopping cart'])[2]/following::th[3]</value>
      <webElementGuid>95be17a7-f6ab-4ed5-95ae-33db4fa06b4c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Price'])[1]/preceding::th[1]</value>
      <webElementGuid>7e390303-4d82-4f6d-9b0c-9237b58576d5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Qty.'])[1]/preceding::th[2]</value>
      <webElementGuid>56e0c484-78c7-4720-a00f-bc086f9af941</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Product(s)']/parent::*</value>
      <webElementGuid>7da0c24b-bf1a-44c8-a2e1-801126837fd2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//th[3]</value>
      <webElementGuid>da0226ce-2b3b-453c-a821-2212d035dfc0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//th[(text() = '
                        Product(s)
                    ' or . = '
                        Product(s)
                    ')]</value>
      <webElementGuid>75cc82a6-2d37-47a3-8b2d-0ac666b6c697</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
