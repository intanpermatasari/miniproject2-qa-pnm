<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_Year</name>
   <tag></tag>
   <elementGuidId>012685bf-be6c-4d8d-be3f-4925f0b47314</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//select[@id='ExpireYear']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#ExpireYear</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>98b87ff0-ed3c-42e1-89d9-7cadee16ae78</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>ExpireYear</value>
      <webElementGuid>b4256a52-8362-4cc4-a785-a71ebdeaaeab</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>ExpireYear</value>
      <webElementGuid>307a8399-b0de-402f-8591-60044c63bf86</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>valid</value>
      <webElementGuid>add04e2f-e09b-413f-bea2-e6fe0bd56559</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>2024
2025
2026
2027
2028
2029
2030
2031
2032
2033
2034
2035
2036
2037
2038
</value>
      <webElementGuid>1582a866-49dc-4fda-bf3e-b537985b889c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ExpireYear&quot;)</value>
      <webElementGuid>3855ed78-2841-4bbc-862c-df1ac672b088</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//select[@id='ExpireYear']</value>
      <webElementGuid>b202d5dd-e37c-4f5d-8400-e8c6ad44e334</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='checkout-payment-info-load']/div/div/div/table/tbody/tr[4]/td[2]/select[2]</value>
      <webElementGuid>319cc121-e1f2-43c1-84ea-001dfa272473</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Expiration date'])[1]/following::select[2]</value>
      <webElementGuid>e775f421-5617-4002-9a5d-cbb093dd9fb7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Card code'])[1]/preceding::select[1]</value>
      <webElementGuid>740502b6-8337-4e12-9550-3fc19da361cd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//select[2]</value>
      <webElementGuid>efc871d2-e3c1-4e2b-b78e-00e3b6a53045</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[@id = 'ExpireYear' and @name = 'ExpireYear' and (text() = '2024
2025
2026
2027
2028
2029
2030
2031
2032
2033
2034
2035
2036
2037
2038
' or . = '2024
2025
2026
2027
2028
2029
2030
2031
2032
2033
2034
2035
2036
2037
2038
')]</value>
      <webElementGuid>e2a3b889-36e6-42b4-8692-1995144c7757</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
