<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_CardNumber</name>
   <tag></tag>
   <elementGuidId>8802196d-ae8e-48a1-adfe-b0785d927996</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='CardNumber']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#CardNumber</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>df737b07-2b86-45cc-a584-a1ec9678b40b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocomplete</name>
      <type>Main</type>
      <value>off</value>
      <webElementGuid>874fcd63-1059-4a24-b623-701df863991a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>CardNumber</value>
      <webElementGuid>5b5b9b62-3ac6-4c03-afa0-140bbd9ba42e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>maxlength</name>
      <type>Main</type>
      <value>22</value>
      <webElementGuid>89be5b06-a23f-4077-b2a5-bab62f1d353a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>CardNumber</value>
      <webElementGuid>d9f68c73-e93e-4d57-885d-3a759dca86cc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>ee359352-9476-45ef-9b7c-365a7edda9ff</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;CardNumber&quot;)</value>
      <webElementGuid>b3889668-9c30-44cf-ada0-0adf9ff63fa3</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='CardNumber']</value>
      <webElementGuid>aa2619e3-5513-4e02-a2ff-5246035b26ad</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='checkout-payment-info-load']/div/div/div/table/tbody/tr[3]/td[2]/input</value>
      <webElementGuid>0199d455-03a6-49c1-8f0d-74632ab57dd4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//tr[3]/td[2]/input</value>
      <webElementGuid>08148e2c-49cd-4c83-9924-0f15754cedda</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@id = 'CardNumber' and @name = 'CardNumber' and @type = 'text']</value>
      <webElementGuid>5842e5dd-e61f-4307-8935-d4b15037cfa0</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
