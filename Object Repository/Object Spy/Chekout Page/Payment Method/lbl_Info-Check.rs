<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>lbl_Info-Check</name>
   <tag></tag>
   <elementGuidId>1a22fe11-91c2-4c07-a5da-c5cabc3003cf</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='checkout-payment-info-load']/div/div/div/table/tbody/tr/td</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>td</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>td</value>
      <webElementGuid>24c15745-e37a-49a2-a306-264a1029c560</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
            Mail Personal or Business Check, Cashier's Check or money order to:Tricentis GmbH Leonard-Bernstein-Straße 10 1220 Vienna AustriaNotice that if you pay by Personal or Business Check, your order may be held for up to 10 days after we receive your check to allow enough time for the check to clear.  If you want us to ship faster upon receipt of your payment, then we recommend your send a money order or Cashier's check.P.S. You can edit this text from admin panel.
        </value>
      <webElementGuid>c5d9c093-c6f9-4011-922e-21e191fd7dbe</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;checkout-payment-info-load&quot;)/div[@class=&quot;checkout-data&quot;]/div[@class=&quot;section payment-info&quot;]/div[@class=&quot;info&quot;]/table[1]/tbody[1]/tr[1]/td[1]</value>
      <webElementGuid>7886149f-a75f-4cf0-9ec8-a94a57fd5ae3</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='checkout-payment-info-load']/div/div/div/table/tbody/tr/td</value>
      <webElementGuid>0f04553a-396f-4ce8-9c8f-73450c147170</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Payment information'])[1]/following::td[1]</value>
      <webElementGuid>5e67567e-47bc-438d-9c52-82eeb4225bcb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Loading next step...'])[4]/following::td[1]</value>
      <webElementGuid>adeff7e8-825d-45c7-a545-37e0e5ff8dd8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//td</value>
      <webElementGuid>bf6d8775-538c-41e8-9171-c14fa480d065</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//td[(text() = concat(&quot;
            Mail Personal or Business Check, Cashier&quot; , &quot;'&quot; , &quot;s Check or money order to:Tricentis GmbH Leonard-Bernstein-Straße 10 1220 Vienna AustriaNotice that if you pay by Personal or Business Check, your order may be held for up to 10 days after we receive your check to allow enough time for the check to clear.  If you want us to ship faster upon receipt of your payment, then we recommend your send a money order or Cashier&quot; , &quot;'&quot; , &quot;s check.P.S. You can edit this text from admin panel.
        &quot;) or . = concat(&quot;
            Mail Personal or Business Check, Cashier&quot; , &quot;'&quot; , &quot;s Check or money order to:Tricentis GmbH Leonard-Bernstein-Straße 10 1220 Vienna AustriaNotice that if you pay by Personal or Business Check, your order may be held for up to 10 days after we receive your check to allow enough time for the check to clear.  If you want us to ship faster upon receipt of your payment, then we recommend your send a money order or Cashier&quot; , &quot;'&quot; , &quot;s check.P.S. You can edit this text from admin panel.
        &quot;))]</value>
      <webElementGuid>55ba94fd-2d9f-4fd7-84e4-8d2cfafb6406</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
