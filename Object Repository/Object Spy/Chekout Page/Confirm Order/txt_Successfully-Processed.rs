<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>txt_Successfully-Processed</name>
   <tag></tag>
   <elementGuidId>80257d84-b2ac-4bfd-a555-6fce08d21311</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Thank you'])[1]/following::strong[1]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>strong</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>strong</value>
      <webElementGuid>16466c72-e947-409e-99c3-91ed31a249c3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Your order has been successfully processed!</value>
      <webElementGuid>4fd870cc-041a-48ab-bf08-c41db8218908</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;master-wrapper-page&quot;]/div[@class=&quot;master-wrapper-content&quot;]/div[@class=&quot;master-wrapper-main&quot;]/div[@class=&quot;center-1&quot;]/div[@class=&quot;page checkout-page&quot;]/div[@class=&quot;page-body checkout-data&quot;]/div[@class=&quot;section order-completed&quot;]/div[@class=&quot;title&quot;]/strong[1]</value>
      <webElementGuid>f1906f69-a346-4eb7-b7e2-f9ad40a4aa5c</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Thank you'])[1]/following::strong[1]</value>
      <webElementGuid>c0b22856-12e1-433d-9c8c-43eb0b7b1517</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Gift Cards'])[2]/following::strong[1]</value>
      <webElementGuid>54f9f783-4222-4510-8a0e-c986dbf5dfed</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Order number: 1623758'])[1]/preceding::strong[1]</value>
      <webElementGuid>bcbbe9e4-b296-4cf3-a33c-2cfb56291964</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Click here for order details.'])[1]/preceding::strong[1]</value>
      <webElementGuid>3228640d-2135-4946-8e7d-195f1eb2318f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Your order has been successfully processed!']/parent::*</value>
      <webElementGuid>14ba35f4-f69b-41ba-87d0-f81d49b84531</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//strong</value>
      <webElementGuid>e89ae4af-5e5c-410c-b028-71adbf6a7234</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//strong[(text() = 'Your order has been successfully processed!' or . = 'Your order has been successfully processed!')]</value>
      <webElementGuid>b7612706-4038-4b06-9a13-38c253c1c315</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
