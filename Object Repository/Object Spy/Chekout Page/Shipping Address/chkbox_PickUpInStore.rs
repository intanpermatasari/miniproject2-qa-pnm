<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>chkbox_PickUpInStore</name>
   <tag></tag>
   <elementGuidId>6d67cddd-404e-4766-b2ee-4c8bfd04d7f8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='PickUpInStore']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#PickUpInStore</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>87d3cd9c-2e9a-4c9f-aea1-4f747e318f23</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>PickUpInStore</value>
      <webElementGuid>d8bf2c73-5182-4741-9f82-0c60743a59bf</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>PickUpInStore</value>
      <webElementGuid>1fc5c41b-9807-4df9-b474-d6a489b202d4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onclick</name>
      <type>Main</type>
      <value>Shipping.togglePickUpInStore(this)</value>
      <webElementGuid>dfc882ec-5c94-49e6-b7ed-785e44f95e05</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>checkbox</value>
      <webElementGuid>88f64d3d-04f0-465b-856b-12592a3d134b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>32d60f33-2023-4c4b-ab60-170f0be138c3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;PickUpInStore&quot;)</value>
      <webElementGuid>489ab125-e2fe-4865-b541-5098cc00579c</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='PickUpInStore']</value>
      <webElementGuid>e9541074-fa77-4a53-b2d6-28124aeecc05</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='checkout-shipping-load']/div/div[2]/p/input</value>
      <webElementGuid>6e7d0476-b022-466d-a1f3-9c9440774e87</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//p/input</value>
      <webElementGuid>79882f7d-5416-448b-9473-757c7526779d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@id = 'PickUpInStore' and @name = 'PickUpInStore' and @type = 'checkbox']</value>
      <webElementGuid>294d834e-2bc1-4e21-8a39-0e0217447d1c</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
