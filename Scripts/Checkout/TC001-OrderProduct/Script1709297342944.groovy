import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://demowebshop.tricentis.com/')

WebUI.click(findTestObject('Object Spy/Home Page/btn_menu-login'))

WebUI.setText(findTestObject('Object Spy/Login Page/input_Email'), 'intan_pnm@gmail.com')

WebUI.setText(findTestObject('Object Spy/Login Page/input_Password'), 'qapnm123')

WebUI.click(findTestObject('Object Spy/Login Page/btn_login'))

WebUI.verifyElementVisible(findTestObject('Object Spy/Home Page/lbl_profile'))

WebUI.setText(findTestObject('Object Record/Search Page/input_product-search'), 'Blue Jeans')

WebUI.click(findTestObject('Object Record/Search Page/btn_Search'))

WebUI.click(findTestObject('Object Record/Search Page/lbl_name-product'))

WebUI.click(findTestObject('Object Record/Cart Page/btn_AddtoCart'))

WebUI.click(findTestObject('Object Record/Cart Page/lbl_menu-shopping-cart'))

WebUI.check(findTestObject('Object Spy/Chekout Page/Shopping Cart/chkbox_ToScheckout'))

WebUI.click(findTestObject('Object Spy/Chekout Page/Shopping Cart/btn_Checkout'))

WebUI.click(findTestObject('Object Spy/Chekout Page/Billing Address/btn_Continue-1.BillingAddress'))

WebUI.click(findTestObject('Object Spy/Chekout Page/Shipping Address/btn_Continue-2.ShippingAddress'))

WebUI.click(findTestObject('Object Spy/Chekout Page/Shipping Method/radio_ShippingMethod-NextDayAir'))

WebUI.click(findTestObject('Object Spy/Chekout Page/Shipping Method/btn__Continue-3.ShippingMethod'))

WebUI.click(findTestObject('Object Spy/Chekout Page/Payment Method/radio_PaymentMethod-COD'))

WebUI.click(findTestObject('Object Spy/Chekout Page/Payment Method/btn__Continue-4.PaymentMethod'))

WebUI.click(findTestObject('Object Spy/Chekout Page/Payment Method/btn__Continue-5.payment-info'))

WebUI.click(findTestObject('Object Spy/Chekout Page/Confirm Order/btn__ConfirmOrder'))

WebUI.verifyElementText(findTestObject('Object Spy/Chekout Page/Confirm Order/txt_Successfully-Processed'), 'Your order has been successfully processed!')

