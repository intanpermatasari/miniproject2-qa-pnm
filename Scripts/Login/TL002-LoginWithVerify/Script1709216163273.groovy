import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

// open browser and navigate to URL
WebUI.openBrowser("https://demowebshop.tricentis.com/")

// go to login page
WebUI.click(findTestObject('Object Repository/Object Spy/Home Page/btn_menu-login'))

// input email and password not valid
WebUI.setText(findTestObject('Object Repository/Object Spy/Login Page/input_Email'),"user1@gmail.com")
WebUI.setText(findTestObject('Object Repository/Object Spy/Login Page/input_Password'), "user123")

// click button login
WebUI.click(findTestObject('Object Repository/Object Spy/Login Page/btn_login'))

// Verify element "Login Error Message"
WebUI.verifyElementVisible(findTestObject('Object Repository/Object Spy/Login Page/Login-ErrorMessage/LoginErrorMessage'), FailureHandling.CONTINUE_ON_FAILURE)

// close browser
WebUI.closeBrowser()