import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

// open Browser
WebUI.openBrowser('')

// navigate to URL
WebUI.navigateToUrl('https://demowebshop.tricentis.com/')

// choose category product
WebUI.click(findTestObject('Object Repository/Object Spy/Home Page/lbl_cat-Books'))

// choose product
WebUI.click(findTestObject('Object Repository/Object Spy/List Product Page/name_Product'))

// click button Add to Cart
WebUI.click(findTestObject('Object Repository/Object Spy/List Product Page/btn_AddToCart'))

// Verify teks "The product has been added to your shopping cart"
WebUI.verifyElementText(findTestObject('Object Repository/Object Spy/Cart Page/txt_CartNotification'),'The product has been added to your shopping cart')

// close Browser
WebUI.closeBrowser()


