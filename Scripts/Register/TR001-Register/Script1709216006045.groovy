import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

// open browser and navigate to url
WebUI.openBrowser("https://demowebshop.tricentis.com/")

// click menu register
WebUI.click(findTestObject('Object Repository/Object Spy/Home Page/btn_menu-register'))

// input form register
WebUI.click(findTestObject('Object Repository/Object Spy/Register Page/radio_Gender_female'))
WebUI.setText(findTestObject('Object Repository/Object Spy/Register Page/input_First name'),"Intan")
WebUI.setText(findTestObject('Object Repository/Object Spy/Register Page/input_Last name'),"Permatasari")
WebUI.setText(findTestObject('Object Repository/Object Spy/Register Page/input_Email'),"intan_pnm@gmail.com")
WebUI.setText(findTestObject('Object Repository/Object Spy/Register Page/input_Password'),"qapnm123")
WebUI.setText(findTestObject('Object Repository/Object Spy/Register Page/input_ConfirmPassword'),"qapnm123")

// click button register
WebUI.click(findTestObject('Object Repository/Object Spy/Register Page/btn_register'))

// close browser
WebUI.closeBrowser()

