<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>TS002-SearchProduct-DDT</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>3</numberOfRerun>
   <pageLoadTimeout>10</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>e33dba80-d66f-48b5-af4f-a315c1bcebd6</testSuiteGuid>
   <testCaseLink>
      <guid>8a333b0d-faeb-4a54-bd7b-0cd5e7fca4f9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Search/TS002-SearchProduct-DDT</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>2a73add7-ecf2-4845-a7e2-88aa6cf87db7</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/Data_Products/Data_Product (1)</testDataId>
      </testDataLink>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId>2a73add7-ecf2-4845-a7e2-88aa6cf87db7</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>product</value>
         <variableId>cb1003b6-be67-45d3-9d27-b1c412b0709b</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
